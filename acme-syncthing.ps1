#!/usr/bin/env pwsh
[CmdletBinding()]
param (
	[Parameter(HelpMessage = 'Silent mode, only errors messages.')]
	[Alias('s')]
	[switch]
	$silent
	,
	[Parameter(HelpMessage = 'Force mode.')]
	[Alias('f')]
	[switch]
	$force
	,
	[Parameter(HelpMessage = 'Common name (primary domain name) of the newly issued certificate.')]
	[Alias("cn")]
	[String]
	$CertCommonName
	,
	[Parameter(HelpMessage = 'Source path.')]
	[Alias('src')]
	[String]
	$SrcDir
	,
	[Parameter(HelpMessage = 'Destination path.')]
	[Alias('dst')]
	[String]
	$DstDir
	,
	[Parameter(HelpMessage = 'Windows service name.')]
	[Alias('srv')]
	[String]
	$ServiceName
)

Set-StrictMode -Version 'Latest'
$Script:ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop
$Script:total_errors = 0

function dQ {
	param (
		[String]
		$text
	)
	if ([string]::IsNullOrEmpty($text) -eq $false ) {
		return [char]34 + $text + [char]34
	} else {
		return [char]34 + [char]34
	}
}

function sQ {
	param (
		[String]
		$text
	)
	if ([string]::IsNullOrEmpty($text) -eq $false ) {
		return [char]39 + $text + [char]39
	} else {
		return [char]39 + [char]39
	}
}

function Write-ErrorObject {
<#
	.SYNOPSIS
		Return string from ErrorRecord object.
#>

	param ([PSObject] $InputObject)

	begin {
		$line = @()
	}

	process {
		$line += 'Error'

		if ($InputObject.InvocationInfo.MyCommand) {
			$line += $InputObject.InvocationInfo.MyCommand.ToString()
		}

		$line += ':'

		if ($InputObject.Exception.Message) {
			$line += $InputObject.Exception.Message.ToString().Trim()
		}

		if ($InputObject.InvocationInfo.ScriptLineNumber) {
			$line += 'line'
			$line += $InputObject.InvocationInfo.ScriptLineNumber.ToString()
		}
	}

	end {
		return ($line -join ' ')
	}
}

function Write-StdErr {
<#
	.SYNOPSIS
		Writes text to stderr when running in a regular console window,
		to the hosts error stream otherwise.

	.DESCRIPTION
		Writing to true stderr allows you to write a well-behaved CLI
		as a PS script that can be invoked from a batch file, for instance.

	Note that PS by default sends ALL its streams to *stdout* when invoked from cmd.exe.

	This function acts similarly to Write-Host in that it simply calls
	.ToString() on its input; to get the default output format, invoke
	it via a pipeline and precede with Out-String.
#>

	param ([PSObject] $InputObject)

	$outFunc = if ($Host.Name -eq 'ConsoleHost') {
		[Console]::Error.WriteLine
	} else {
		$Host.UI.WriteErrorLine
	}

	if ($InputObject) {
		[void] $outFunc.Invoke($InputObject.ToString())
	} else {
		[string[]] $lines = @()
		$Input | ForEach-Object { $lines += $_.ToString() }
		[void] $outFunc.Invoke($lines -join [Environment]::NewLine)
	}
}

function Copy-ModifiedFile {
	[CmdletBinding()]
	param (
		# Path from source file
		[Parameter(
			Mandatory=$true,
			Position=0,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path from source file.")]
		[Alias("s", "src")]
		[ValidateNotNullOrEmpty()]
		[string]
		$srcfile,
		# Path to destination file
		[Parameter(
			Mandatory=$true,
			Position=1,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path to destination file.")]
		[Alias("d", "dst")]
		[ValidateNotNullOrEmpty()]
		[string]
		$dstfile
	)

	# $fileName = ''
	# $file = [System.io.File]::Open($fileName, 'Open', 'Read', 'None'); Write-Host "Press any key to continue ..."; $null = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown"); $file.Close()

	try {
		if (! $(Test-Path -PathType Leaf -LiteralPath $dstfile)) {
			if (! $(Test-Path -PathType Container -LiteralPath $(Split-Path -Parent -Path $dstfile))) {
				New-Item -Force -ItemType Directory -Path $(Split-Path -Parent -Path $dstfile) | Out-Null
			}
			Copy-Item -Force -LiteralPath $srcfile -Destination $dstfile
			return $true
		} else {
			if ($(Get-FileHash -LiteralPath $srcfile).Hash -ne $(Get-FileHash -LiteralPath $dstfile).Hash) {
				Copy-Item -Force -LiteralPath $srcfile -Destination $dstfile
				return $true
			}
		}
		return $false
	}
	catch {
		return $_
	}
}

function Copy-Write {
	[CmdletBinding()]
	param (
		# Path to source file
		[Parameter(
			Mandatory=$true,
			Position=0,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path to source file.")]
		[Alias('s', 'src')]
		[ValidateNotNullOrEmpty()]
		[string]
		$srcfile,
		# Path to destination file
		[Parameter(
			Mandatory=$true,
			Position=1,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Path to destination file.")]
		[Alias('d', 'dst')]
		[ValidateNotNullOrEmpty()]
		[string]
		$dstfile
	)

	New-Variable -Name r -Force -ErrorAction SilentlyContinue
	$r = Copy-ModifiedFile -src $srcfile -dst $dstfile

	if ($r.GetType().Name -eq 'ErrorRecord') {
		Write-ErrorObject $r | Write-StdErr
	} elseif ($r -eq $true) {
		if (! $silent) {
			Write-Host ("copy " + $srcfile + " -> " + $dstfile)
		}
	}

	return $r
}

function Copy-Handler {
	[CmdletBinding()]
	param (
		# Source path
		[Parameter(
			Mandatory=$true,
			Position=0,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Source path.")]
		[Alias('s')]
		[ValidateNotNullOrEmpty()]
		[string]
		$src,
		# Destination path
		[Parameter(Mandatory=$true,
			Position=1,
			ParameterSetName="path",
			ValueFromPipeline=$true,
			ValueFromPipelineByPropertyName=$true,
			HelpMessage="Destination path.")]
		[Alias('d')]
		[ValidateNotNullOrEmpty()]
		[string]
		$dst
	)

	$Result = @()

	# file
	if ($dst.EndsWith([IO.Path]::DirectorySeparatorChar)) {
		$dst = Join-Path -Path $dst -ChildPath $(Split-Path -Leaf -Path $src)
	}

	New-Variable -Name r -Force -ErrorAction SilentlyContinue
	$r = Copy-Write -src $src -dst $dst
	if ($r.GetType().Name -eq 'ErrorRecord') {
		$Result += [PSObject]@{src = $src; dst = $dst; result = $null}
	} elseif ($r -eq $true) {
		$Result += [PSObject]@{src = $src; dst = $dst; result = $true}
	} else {
		$Result += [PSObject]@{src = $src; dst = $dst; result = $false}
	}

	return $Result
}

<#
	Preamble

	Copy certificate pemfilespath\{CommonName}-chain.pem -> USERPROFILE\syncthing\AppData\Local\Syncthing\https-cert.pem 
	Copy private key pemfilespath\{CommonName}-key.pem -> USERPROFILE\syncthing\AppData\Local\Syncthing\https-key.pem
	Restart windows service Syncthing

	Store PemFiles https://www.win-acme.com/reference/plugins/store/pemfiles
#>

$renew = $false

$src = $([IO.Path]::Combine($SrcDir, "$CertCommonName-chain.pem"))
$dst = $([IO.Path]::Combine($DstDir, 'https-cert.pem'))
if ($(Test-Path -LiteralPath $src)) {
	$r = Copy-Handler -src $src -dst $dst
	$r | ForEach-Object { if ([string]::IsNullOrEmpty($_.result)) {$total_errors++} ; if ($_.result) {
			$renew = $true
		}
	}
} else {
	"Source file $(dQ($src)) not exist." | Write-StdErr
	$total_errors++
	exit 2
}

$src = $([IO.Path]::Combine($SrcDir, $CertCommonName + '-key.pem'))
$dst = $([IO.Path]::Combine($DstDir, 'https-key.pem'))
if ($(Test-Path -LiteralPath $src)) {
	$r = Copy-Handler -src $src -dst $dst
	$r | ForEach-Object { if ([string]::IsNullOrEmpty($_.result)) {$total_errors++} ; if ($_.result) {
			$renew = $true
		}
	}
} else {
	"Source file $(dQ($src)) not exist." | Write-StdErr
	$total_errors++
	exit 4
}

if ($force) {
	$renew = $true
}

if ($renew -eq $true) {
	if (! $silent) {
		Write-Host "Certificate renew."
	}
	try {
		$Service = Get-Service -Name $ServiceName -ErrorAction Stop
	}
	catch {
		"Error get windows service $(sQ($ServiceName))." | Write-StdErr
		$total_errors++
		exit 8
	}
	if ($Service.Status -eq "Running") {
		if (! $silent) {
			Write-Host "It is necessary to restart the $(sQ($ServiceName)) service."
		}
		try {
			Restart-Service -Force -Name $ServiceName -ErrorAction Stop
		}
		catch {
			"Error restart windows service $(sQ($ServiceName))." | Write-StdErr
			$total_errors++
			exit 16
		}
	}
}

# final
if ($total_errors -eq 0) {
	exit 0
} else {
	exit 1
}
